import React, {Component} from 'react';

import './App.css';

import {store} from './Store';
import {TodoList} from './TodoList';
import {Header} from './Header';

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    window.initRipple();
  }

  render() {
    return <div className="App">
      <Header store={store} />
      <TodoList store={store} />
    </div>;
  }
}
