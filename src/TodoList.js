import React, {Component} from 'react';
import {observer} from 'mobx-react';

export const TodoList = observer(class TodoList extends Component {
  componentDidMount() {
    window.initRipple();
  }

  render() {
    const {filterTodos} = this.props.store;

    const todoLis = filterTodos.map(todo => (
      <li className="mdc-list-item TodoItem" role="option" aria-selected={false} key={todo}>
        <span className="mdc-list-item__text">{todo}</span>
      </li>
    ));

    const noLis = [
      <li className="mdc-list-item TodoItem" role="option" aria-selected={false} key={1}>
        <span className="mdc-list-item__text">{'no item found'}</span>
      </li>
    ];

    return <div className={'TodoList p-b-15'}>
      <div className={'p-t-15 p-h-16'}>
        <ul className="mdc-list my-list" role="listbox">{todoLis.length ? todoLis : noLis}</ul>
      </div>
    </div>;
  }
});