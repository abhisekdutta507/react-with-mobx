import React, {Component} from 'react';
import {observer} from 'mobx-react';

export const Header = observer(class Header extends Component {
  filter(e) {
    this.props.store.filter = e.target.value;
  }

  componentDidMount() {
    window.initRipple();
  }

  render() {
    const {filter} = this.props.store;

    return <>
      <div className={'Header p-b-15 p-h-16'}>
        <p className={'w-100 list-heading'}>List of states in India</p>

        <div className="mdc-text-field mdc-text-field--outlined p-h-16 w-100">
          <input type="text" className={'filter mdc-text-field__input'} value={filter} onChange={this.filter.bind(this)} />
          <div className="mdc-notched-outline">
            <div className="mdc-notched-outline__leading"></div>
            <div className="mdc-notched-outline__notch">
              <label htmlFor="tf-outlined" className="mdc-floating-label">Search</label>
            </div>
            <div className="mdc-notched-outline__trailing"></div>
          </div>
        </div>

      </div>
    </>;
  }
});